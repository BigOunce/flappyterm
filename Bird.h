#include <string>

namespace fterm
{
	class Bird
	{
		unsigned int yposition;

		static unsigned int getkeypress_internal();

		int frames_left; //countdown for moving up

		unsigned int framelimit = 5;

		std::string bird_placeholder = "@";

		public:

		Bird(unsigned int yposition);

		void modyposition();

		unsigned int getkeypress();

		unsigned int calcmod();

		unsigned int getyposition();

		void new_getkeypress();
	};
}
