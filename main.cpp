#include <ncurses.h>

#include <iostream>
#include <stdlib.h>

#include <thread>
#include <chrono>

#include "ViewDrawer.h"
#include "Bird.h"

using namespace std::chrono_literals;

void updatescreen(fterm::ViewDrawer * drawer, fterm::Bird * bird)
{
	clear();


	for(unsigned int i = 0; i < drawer->screenbuffer.size(); ++i)
	{
		move(i,0);
		for(unsigned int j = 5; j < drawer->screenbuffer[i].size() - 5; ++j)
		{
			if(i == bird->getyposition() && j == drawer->cols/2)
			{
				printw("@");
			}
			else
				printw(drawer->screenbuffer[i][j].c_str());
		}
	}

	/*move(drawer->rows/2,drawer->cols/2);

	printw("@");*/

	//refresh();
}


int main(int argc, char ** argv)
{
	//initialise sequence

	initscr();

	timeout(50);

	unsigned int rows, cols;

	getmaxyx(stdscr,rows,cols);

	//std::cout << "rows: " << rows << " cols: " << cols << "\n";

	fterm::ViewDrawer drawer(rows, cols);

	fterm::Bird bird(cols/2);

	clear();
	
	refresh();


	while(1)
	{
		drawer.UpdatePipeScreen();

		bird.modyposition();
		updatescreen(&drawer,&bird);


		//put in bird updates here, and sort out timer

		std::this_thread::sleep_for(50ms);
	}

	endwin();
	
	return 0;
}
