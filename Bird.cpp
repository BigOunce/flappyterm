#include "Bird.h"

#include <thread>
#include <chrono>
#include <ncurses.h>
#include <thread>
#include <iostream>

using namespace std::chrono_literals;

fterm::Bird::Bird(unsigned int yposition)
{
	this->yposition = yposition;
	
	this->frames_left = 0;
}
unsigned int fterm::Bird::getkeypress_internal()
{
	unsigned int keypress = 0;
	timeout(50);
	char getchresult = wgetch(stdscr);
	if(getchresult != ERR)
		keypress = 1;

	printw(std::to_string(getchresult).c_str());
	//std::cout << "KEYPRESS: " << keypress;
	//std::this_thread::sleep_for(1000ms);

	return keypress;
}


/*unsigned int fterm::Bird::getkeypress()
{
	unsigned int toreturn = 0;
	
	std::future<unsigned int> future = std::async(this->getkeypress_internal);

	if(future.wait_for(35ms) == std::future_status::ready)
	{
		toreturn = future.get();
	}
	return toreturn;
}*/

void fterm::Bird::modyposition()
{
	unsigned int keypressed = 0;

	if(this->getkeypress_internal())
	{
		this->frames_left = this->framelimit;
	}
	if(this->frames_left > 0) //while the countdown is valid
	{
		this->yposition++;
		this->frames_left--;
	}
	else
	{
		this->yposition--;
	}
}

unsigned int fterm::Bird::getyposition()
{
	return this->yposition;
}
