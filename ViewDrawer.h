#include <vector>
#include <string>


namespace fterm
{
	class ViewDrawer
	{

		std::string verticalbar = "|";

		std::string underleftcorner = "-";

		std::string underrightcorner = "-";

		std::string topleftcorner = "-";

		std::string toprightcorner = "-";
		
		std::string horizontalbar = "-";



		unsigned int current_pipe_width; //used to determine how much is drawn currently
		
		unsigned int current_gap_between_pipes;

		unsigned int current_pipe_centre;;

		//for now, have a pipe be of width 5 and a gap of 10

		public:

		unsigned int rows, cols;

		std::vector<std::vector<std::string>> screenbuffer;


		void UpdatePipeScreen();

		ViewDrawer(unsigned int termwidth, unsigned int termheight);

		void calculate_new_pipecentre();

#ifdef DEBUG
		unsigned int debug_printscreens();
#endif
	};
}
