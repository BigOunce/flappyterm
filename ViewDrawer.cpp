#include "ViewDrawer.h"

#include <vector>
#include <string>
#include <iostream>
#include <random>



fterm::ViewDrawer::ViewDrawer(unsigned int rows, unsigned int cols)
{
	for(unsigned int i = 0; i < rows; ++i)
	{
		std::vector<std::string> topush;
		for(unsigned int i = 0; i < cols + 10; ++i)
			topush.push_back(" ");
		this->screenbuffer.push_back(topush);
	}

	this->rows = rows;
	this->cols = cols;

	this->current_pipe_centre = this->screenbuffer.size()/2;

	this->current_gap_between_pipes = 0;

	this->current_pipe_width = 1;
};

void fterm::ViewDrawer::calculate_new_pipecentre()
{
	int difference;
	std::random_device device;
	std::mt19937 generator(device());
	std::uniform_int_distribution<int> distributor(-5,5);

	difference = distributor(generator);

	this->current_pipe_centre += difference;

	if(this->current_pipe_centre <= 5)
		this->current_pipe_centre += 5;
	else if(this->current_pipe_centre >= this->screenbuffer.size() - 5)
		this->current_pipe_centre -= 5;
}


void fterm::ViewDrawer::UpdatePipeScreen()
{
#ifdef DEBUG
	std::cout << "started update";
#endif
	//first, remove the start of the buffer.
	for(unsigned int i = 0; i < this->screenbuffer.size(); ++i)
	{
		std::vector<std::string>::iterator start = this->screenbuffer[i].begin();
		this->screenbuffer[i].erase(start);
	}


#ifdef DEBUG
	std::cout << "removed";
#endif

	//then, start drawing the pipe
	
	if(this->current_gap_between_pipes == 11) //start drawing
	{
#ifdef DEBUG
		std::cout << "started to draw";
#endif
		if(this->current_pipe_width == 6)
		{
			this->current_pipe_width = 1;
			this->calculate_new_pipecentre();
		}

		//current implementation will suck.
		
		//calculation of new centre will go here.

		if(this->current_pipe_width == 1) //start of pipe
		{
#ifdef DEBUG
			std::cout << "starting pipe\n";
#endif
			for(unsigned int i = 0; i < this->screenbuffer.size(); ++i)
			{
				if(i > this->current_pipe_centre - 5 && i < this->current_pipe_centre + 5 )
					this->screenbuffer[i].push_back(" ");
				else if(i == current_pipe_centre - 5) //under left corner
					this->screenbuffer[i].push_back(this->underleftcorner);
				else if(i == current_pipe_centre + 5) //top left corner
					this->screenbuffer[i].push_back(this->topleftcorner);
				else
					this->screenbuffer[i].push_back(this->verticalbar);
			}
			this->current_pipe_width++;
		}
		else if(this->current_pipe_width < 5) //middle of pipe
		{
#ifdef DEBUG
			std::cout << "bigchungus";
#endif
			for(unsigned int i = 0; i < this->screenbuffer.size(); ++i)
			{
				if(i == this->current_pipe_centre - 5 || i == this->current_pipe_centre + 5) // if not the edge of the pipe
				{
					this->screenbuffer[i].push_back(this->horizontalbar);
				}
				else
					this->screenbuffer[i].push_back(" ");
			}
			this->current_pipe_width++;
		}
		else if(this->current_pipe_width == 5) //end of pipe
		{
#ifdef DEBUG
			std::cout << "wholesome 100";
#endif
			for(unsigned int i = 0; i < this->screenbuffer.size(); ++i)
			{
				if(i > this->current_pipe_centre - 5 && i < this->current_pipe_centre + 5)
					this->screenbuffer[i].push_back(" ");
				else if(i == current_pipe_centre - 5) //under right corner
					this->screenbuffer[i].push_back(this->underrightcorner);
				else if(i == current_pipe_centre + 5) //top left corner
					this->screenbuffer[i].push_back(this->toprightcorner);
				else
					this->screenbuffer[i].push_back(this->verticalbar);
			}
			this->current_gap_between_pipes = 0; //should have finished drawing
			this->current_pipe_width++;
		}
	}
	else
	{
		for(unsigned int i = 0; i < this->screenbuffer.size(); ++i)
		{
			this->screenbuffer[i].push_back(" ");
		}
		this->current_gap_between_pipes++;
	}
}


#ifdef DEBUG
unsigned int fterm::ViewDrawer::debug_printscreens()
{
	for(unsigned int i = 0; i < this->cols; ++i)
	{
		for(unsigned int j = 0; j < this->rows; ++j)
		{
			std::cout << i << " " << j << "\n";
			this->screenbuffer[i][j];
		}

		std::cout << "\n";
	}
	return 0;
}
#endif
