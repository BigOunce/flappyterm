
DEBUG :=



flappyterm: main.o ViewDrawer.o Bird.o
	g++ -o flappyterm main.o -lncurses ViewDrawer.o Bird.o

ViewDrawer.o:
	g++ -g $(DEBUG)-c ViewDrawer.h ViewDrawer.cpp

Bird.o:
	g++ -g $(DEBUG) -c Bird.h Bird.cpp

main.o: main.cpp
	g++ -g $(DEBUG)-c  main.cpp

debug:
	make DEBUG=-DDEBUG

clean:
	rm *.o
	rm *.gch
	rm flappyterm
